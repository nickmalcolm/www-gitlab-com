---
layout: handbook-page-toc
title: GitLab Learn Implementation
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# Implementation of the EdCast LXP


## Go Live Dates and Rolling Wave Plan

GitLab has created a three phase plan for the implementation of the EdCast LXP.

| Phase | Timeline | Audience | Status |
| ----- | ----- | ----- | ----- |
| Phase 1 | Q4FY21 | Field certification program (GitLab Account Executives (AEs), Strategic Account Leaders (SALs), Solution Architects (SAs), and Technical Account Managers (TAMs) | Complete |
| Phase 2 | Q1FY22 | All GitLab team members | Completed |
| Phase 3 | Q1FY22 | GitLab customers and wider community members | In progress |
| Phase 4 | TBD | GitLab Partners migratie from Impartner LMS to EdCast LXP | In progress |
| Phase 5 | TBD | GitLab for Education program members |


## Business Readiness

Review the slides below to understand how the GitLab team is organizing our business readiness plan for the LXP launch.

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/1TY-gmS_-IPvYBh1FeJyK6qAyNtcikLx799UMluwUYnQ/edit#slide=id.gaedb85dfe7_1_98" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>


## Project Management using GitLab

The LXP Implementation team uses [GitLab issue boards](https://gitlab.com/gitlab-com/people-group/learning-development/edcast-lxp-implementation/-/boards) and [GitLab Epics](https://gitlab.com/groups/gitlab-com/-/epics/918) to track and collaborate on the roll out of the EdCast LXP.






